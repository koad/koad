### Hi there 👋

![Keybase PGP](https://img.shields.io/keybase/pgp/koad?style=for-the-badge)

[Chat with me on keybase](https://keybase.io/koad/chat) or [find me in the matrix](https://matrix.to/#/@matrix:koad.sh)


# 🤔 Consider the following...

## 🔭 Cypherpunk

A cypherpunk is any individual advocating widespread use of strong cryptography and privacy-enhancing technologies as a route to social and political change. 

[A Cypherpunk's Manifesto](https://www.activism.net/cypherpunk/manifesto.html)

## 🌱 Optimist

An optimist's motto is "Friend of Youth" and "Bringing Out the Best in Youth, in our Communities, and in Ourselves."

[The Optimist Creed](https://www.optimist.org/member/creed.cfm)

## 💬 King of all Data

A king of all data is an individual who reaches a certain level of mastery in the art of organizing his digital creations; off and on-line.

[kingofalldata.com](https://kingofalldata.com)

